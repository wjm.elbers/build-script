= CLARIN-ERIC docker build workflow
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 💡 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

This project contains all scripts and docker images which are required for the CLARIN-ERIC docker workflow.

== Dependencies

[options="header",cols=",,,m"]
|===
| Conditions | Type | Name (URL) | Version constraint

| by necessity
| software
| https://www.docker.com/[Docker Compose]
| ==1.8.0

| by necessity
| software
| https://www.docker.com/[Docker Engine]
| ==1.11.2

| for building, testing and releasing
| image
| https://gitlab.com/CLARIN-ERIC/build-image[CLARIN Build image]
| ==1.3.0

| for Docker image vulnerability scan
| image
| https://gitlab.com/CLARIN-ERIC/docker-snyk-cli.[CLARIN Docker Snyk client image CI]
| ==0.0.4

| for Dockerfile lint
| image
| https://hub.docker.com/r/hadolint/hadolint.[hadolint Image on Docker hub]
| ==2.8.0-alpine

| for shell script lint
| image
| https://registry.hub.docker.com/r/koalaman/shellcheck-alpine.[shellcheck-alpine Image on Docker hub]
| ==v0.8.0

| for testing
| image
| https://gitlab.com/CLARIN-ERIC/docker-tester.[CLARIN Docker HTTP test image]
| ==1.4.0

| for CI integration
| platform
| https://about.gitlab.[GitLab CI]
| ==8.10.4

|===

== Goals

Provide a uniform build, test and release workflow, both locally and within the gitlab platform
which allows customization where needed.

== Installing

=== Automated
Install the build script in the project directory (not yet git initialized!) with the following, `curl pipe bash`, command:

[source,sh]
----
curl -s -L https://gitlab.com/CLARIN-ERIC/build-script/raw/master/init_repo.sh | bash
----

Note: this command can also be used to update and existing repo to the latest version.

=== Manual initialization

In the following sub-sections you'll find a breakdown of the commands run by the script.

==== Initialize git local repo with https://gitlab.com/CLARIN-ERIC/build-script[build-script] as submodule

[source,sh]
----
VERSION=2.0.19 && \
git init && \
git submodule add https://gitlab.com/CLARIN-ERIC/build-script.git build-script && \
cd build-script && \
git reset --hard ${VERSION} && \
cd .. && \
ln -s build-script/build.sh build.sh && \
ln -s build-script/copy_data_noop.sh copy_data.sh && \
ln -s build-script/update_version_noop.sh update_version.sh && \
ln -s build-script/init_repo.sh update_build_script.bash && \
cp build-script/_gitlab-ci_default.yml .gitlab-ci.yml
----

In order to update with changes from the submodule remote, issue the following command from the repository root:

[source,sh]
----
git submodule update --recursive --remote
----

==== Prepare the directory structure

[source.sh]
----
mkdir image run test
touch image/.gitkeep
touch run/.gitkeep
touch test/.gitkeep
----

==== Update local git repo

[source,sh]
----
git add .
git commit -m "Intialized empty repo with build script v${VERSION}"
----

=== Output

Running the automated `curl pipe bash` command or doing it manually should result in a directory structure similar to
the following (hidden files not shown):

[source, sh]
----
.
├── build-script
│   ├── LICENSE.txt
│   ├── README.adoc
│   ├── build.sh
│   └── copy_data_noop.sh
├── build.sh -> build-script/build.sh
├── copy_data.sh -> build-script/copy_data_noop.sh
├── image
└── run

3 directories, 6 files
----

After adding files to the `image` and `run` directories, the `.gitkeep` files can be removed. However it does no harm
to leave them be.

== Upgrading

From the parent project root directory, run:

[source,sh]
----
git submodule update --recursive --remote
----


== Customizing

=== Build parameters

You can provide custom values for some of the parameters used in the build process by placing a `variables.sh` file in
the root of your repository.

Possible values:
[source,sh]
----
IMAGE_DIR="your new image directory/"           #Make sure this ends with a "/"
----

=== Docker build arguments

You define arguments to be passed to the `docker build` command executed for builds in the
CI environment. To do so, set an environment variable `GITLAB_DOCKER_ARGS`. You can do
this in the `build.script` property of your `.gitlab-ci.yml` file.

One reason to use this, is to make values from the CI 
https://docs.gitlab.com/ee/ci/variables/predefined_variables.html[predefined variables]
available in the build context. For instance:

..gitlab-ci.yml
[source,sh]
----
build:
  artifacts:
    untracked: true
  script: >
    GITLAB_DOCKER_ARGS="--build-arg TAG_OR_BRANCH=$CI_COMMIT_REF_NAME"
    timeout 1440
    bash -x ./build.sh --build
  stage: build
  tags:
    - docker
----

In this example, the `TAG_OR_BRANCH` argument can be defined and will be set in the 
Dockerfile:

.Dockerfile
[source]
----
...
ARG TAG_OR_BRANCH
...
RUN echo "Tag or branch: ${TAG_OR_BRANCH}"
...
----

[IMPORTANT]
Be aware that the build script will also pass arguments to the `docker build` command, so
make sure that there are no potential clashes when using this option.

=== Linting

Specific directories can be excluded from the shell script linting process by creating a `.linting` file in the project
root:

[source]
----
LINT_SHELL_EXCLUDE_DIRS="dir1;dir2 with spaces"
----

This file should contain a variable `LINT_SHELL_EXCLUDE_DIRS` with a string value containing a list of directory names
separated by a semicolon (`;`). Any `.sh` or `.bash` script under these directories will not be linted.

If no `.linting` file exists all files under the project root will be linted by default.

=== Testing

In order to test your dockerized environment a `docker-compose.yml` file should be created in the `test` subdirectory.
Within this compose file the services to test together with a test suite should be defined and after running all tests all
containers should exit succsfully (exit code 0) for the test to pass.

Example for any image exposing an HTTP(s) endpoint: (the following test setup is instantiated by default when creating a 
new repository with `init_repo.sh` and will work out-of-the-box for any image descending from CLARIN docker Alpine Supervisor Base image)

[source,sh]
----
version: '3.1'
services:
  base:
      image: "${IMAGE_QUALIFIED_NAME}"
      command: --test
      volumes:
        - "test:/test:rw"
      restart: "no"
      logging:
        driver: json-file

  tester:
    image: "registry.gitlab.com/clarin-eric/docker-tester:1.4.0-multiarch-rc2"
    command: http multi -v -k -T 1440
    volumes:
      - "../test/checker.conf:/etc/checker.conf"
      - "test:/test"
    restart: "no"
    logging:
      driver: json-file

volumes:
  test:
    external: false
----

Note that:

- the `${IMAGE_QUALIFIED_NAME}` variable is set within the build script to reference the latest version of your image.
- by using `command: --test` the services knows to shut down when all tests are finished by monitoring the shared `/test` volume, which is managed by docker compose (`external: false`).
- checker.conf contains a list of URLs paired with expected response codes to test:

[source,sh]
----
https://nginx/;200
https://nginx/localhost/;404
https://nginx/50x.html;200
----

== To use
[IMPORTANT]
.Cloning created repositories
====
In order to clone a project repository created by this build script and include the build script submodule files, use the '--recursive' parameter.
[source,sh]
git clone your_project_url.git --recursive
====

[source,sh]
----
build.sh [-bdhlnRrtvs(ld)(ls)]

  -b,  --build        Build docker image
  -r,  --release      Push docker image to registry
  -t,  --test         Execute tests
  -ld, --lint-docker  Lint Dockerfile under <image directory>/Dockerfile
  -ls, --lint-shell   Lint shell scripts under <image directory>/[*.sh, *.bash]
  -s,  --scan         Perform static vulnerability scanning on image
                       Make sure the SNYK_TOKEN variable is set (export SNYK_TOKEN=<token value>)
  -R,  --run          Run (only works in local (-l, --local) mode)
  -d,  --down         Down (only works in local (-l, --local) mode)

  -c,  --no-cache     Ignore docker cache during build (-b) step
  -l,  --local        Run workflow locally in a local docker container
  -v,  --verbose      Run in verbose mode
  -n,  --no-export    Don't export the build artiface, this is used when running
                       the build workflow locally

  Environment variables:
      BUILD_PLATFORMS: Docker buildx target platforms. (Default:"linux/amd64,linux/arm64")

  -h,  --help         Show help
----

=== Managing external data

During image building external data (e.g. releases) is often needed. In order to accomodate fetching external data the
copy_data.sh script has been provided. Two methods are defined in this script:

[source,sh]
----
#!/bin/bash

init_data (){
    LOCAL=0
    if [ "$1" == "local" ]; then
        LOCAL=1
    fi

    if [ "${LOCAL}" -eq 0 ]; then
        #Remote / gitlab ci
        echo -n ""
    else
        #Local copy
        echo -n ""
    fi
}

cleanup_data () {
    echo -n ""
}
----

As you can see `init_data` supports two scenarios. one for local copy actions and one for gitlab ci integrated copy
actions. This distinction is typically used to download releases (e.g. from b2drop) during gitlab ci workflows and to
copy in local files during local build / development cycles.

`cleanup_data` should implement cleanup commands to remove all files created / downloaded during the `init_data` phase.

An example can be found here: https://gitlab.com/CLARIN-ERIC/docker-aai-discovery/blob/master/copy_data.sh.

=== Customizing

A number of variables are supported to customize the docker build process. Any of these variables will be passed in to
the docker build command and can be used in the docker file via the `ARG` directive.

[source,sh]
----
 --build-arg VARIABLE=${VARIABLE}"
----

The following variables are supported:

* DIST_VERSION

=== Locally

When running the build script locally all tasks are run using a docker-in-docker environment with the image:
https://gitlab.com/CLARIN-ERIC/build-image as defined
in the build script (```build.sh```).

==== Linting

[source,sh]
----
sh build.sh --lint-docker --local
sh build.sh --lint-shell --local
----

==== Building

[source,sh]
----
sh build.sh --build --local
----

===== Disable docker cache
To disable the docker cache during the build

[source,sh]
----
sh build.sh --build --local --no-cache
----

==== Testing

[source,sh]
----
sh build.sh --test --local
----

==== Releasing

[source,sh]
----
sh build.sh --release --local
----

==== Scanning for known vulnerabilities (Snyk)
Make sure the SNYK_TOKEN variable is set (export SNYK_TOKEN=<token value>)

[source,sh]
----
sh build.sh --scan --local
----

==== Run multiple build tasks
The various tasks offered by `build.sh` may be called together. e.g. 

----
sh build.sh --lint-docker --lint-shell --build --test --release --scan --local --no-cache
----

Independently of the order the various task parameters are passed, the build script will always run them according
to the following sequence:

lint-shell-->lint-docker-->build-->test-->release-->scan

The script will be interrupted if any of the tasks exits with an error code.


=== Integrated in GitLab CI

When building remotely (within GitLab CI), the environment specified in the ```.gitlab-ci.yml``` file is used.

To integrate GitLab CI copy and customize the default GitLab CI template file ```_gitlab-ci_default.yml``` to ```.gitlab-ci.yml```
in your project root:

[source,sh]
----
cp build-script/_gitlab-ci_default.yml .gitlab-ci.yml
----

Note that all scripts are run with a predefined timeout of 720 seconds. If the 
timeout is exceeded the job will typically exit with a `code 143`. If this happens
increase the timeout values as needed.

==== Resolving FATAL: too large errors 

If total size of artifacts shared in a stage exceeds the GitLab.com 1GB limit, 
your pipeline will fail with the following error:

[source,sh]
----
ERROR: Uploading artifacts as "archive" to coordinator... too large archive  id=9999999999 responseStatus=413 Payload Too Large status=413 token=some_token
FATAL: too large                    
----

This is amplified after the build script got support for multiarch builds and shares the docker 
registry between CI stages in order to speed up the pipeline. A `200MB` image will take up 
`2 * 200 = 400MB` when building for two architectures, `200 * 3 = 600MB` when building 3 
architectures, ....

In order to circumvent this multiplication of artifact size in relation to the number of build 
architectures, the sharing of the docker registry between stages can be disabled. This will slow down
the build process a bit, but also reduce the artifact size.

[source,sh]
----
build:
  artifacts:
    untracked: false
    paths:
      - /builds/CLARIN-ERIC/docker-<name>/output/docker-<name>:*.tar.gz
  script: timeout 1440 bash -x ./build.sh --build 
  stage: build
  tags:
    - docker
----
Replace `<name>` with the name of your project.

Don't forget to remove or set `untracked: false`.
